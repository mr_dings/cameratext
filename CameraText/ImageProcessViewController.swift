//
//  ImageProcessViewController.swift
//  CameraText
//
//  Created by Suen Yuen on 4/5/2017.
//  Copyright © 2017 Suen Yuen. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

class ImageProcessViewController: UIViewController {

    @IBOutlet weak var toolbar: UIToolbar!
    @IBOutlet weak var imagePreview: UIImageView!
    var data: Data?
    var previewImage: UIImage?
    var originImage: UIImage?
    var inString: NSString?
    
    var photoBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIApplication.shared.isStatusBarHidden = true
        self.toolbar.setBackgroundImage(UIImage(), forToolbarPosition: .any, barMetrics: .default)
        self.toolbar.setShadowImage(UIImage(), forToolbarPosition: .any)
//        self.toolbar.barTintColor = UIColor(colorLiteralRed: 128, green: 128, blue: 128, alpha: 128)
        
        previewImage = UIImage(data: data!)
        originImage = UIImage(data: data!)
        
        print("\(String(describing: originImage?.size.width)) : \(String(describing: originImage?.size.height))")
        
        previewImage = textToImage(drawText: "test string", inImage: previewImage!)
        imagePreview.image = previewImage
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.presentAlert()
    }
    
    @IBAction func showAlert(_ sender: UIBarButtonItem) {
        presentAlert()
    }
    func presentAlert() {
        
        var userIdTextField: UITextField?
        
        // Declare Alert message
        let dialogMessage = UIAlertController(title: "change text", message: "text change to ", preferredStyle: .alert)
        
        // Create OK button with action handler
        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
            print("Ok button tapped")
            
            if let userInput = userIdTextField?.text {
                print("User entered \(userInput)")
                self.inString = userInput as NSString
                let queue = DispatchQueue.global()
                queue.asyncAfter(deadline: .now() + 0.2, execute: {
                    print("async start")
                    let activityView = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
                    activityView.center = self.view.center
                    
                    DispatchQueue.main.async {
                        activityView.startAnimating()
                        self.view.addSubview(activityView)
                    }
                    self.previewImage = self.textToImage(drawText: self.inString!, inImage: self.originImage!)
                    print("task complete, start update UI")
                    DispatchQueue.main.async {
                        self.imagePreview.image = self.previewImage
                        print("updata UI complete")
                        print("async all finish")
                        activityView.stopAnimating()
                        activityView.removeFromSuperview()
                    }
                })
            }
        })
        
        // Create Cancel button with action handlder
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in
            
        }
        
        //Add OK and Cancel button to dialog message
        dialogMessage.addAction(ok)
        dialogMessage.addAction(cancel)
        
        // Add Input TextField to dialog message
        dialogMessage.addTextField { (textField) -> Void in
            
            userIdTextField = textField
            userIdTextField?.placeholder = "text"
        }
        
        // Present dialog message to user
        self.present(dialogMessage, animated: true, completion: nil)
    }
    
    func textToImage(drawText text: NSString, inImage image: UIImage) -> UIImage {
        let textColor = UIColor.red
        
//        let newCgIm = image.cgImage!.copy()
//        let mImage = UIImage(cgImage: newCgIm!, scale: image.scale, orientation: image.imageOrientation)
//        previewImage = mImage

        let heightInPoints = previewImage?.size.height
        let widthInPoints = previewImage?.size.width
        let nWidth = UIScreen.main.nativeBounds.width
        let nHeight = UIScreen.main.nativeBounds.height
        let Xfactor = widthInPoints! / nWidth
        let Yfactor = heightInPoints! / nHeight
        
        let fontSize = 12 * Xfactor * UIScreen.main.scale
        let textFont = UIFont(name: "Helvetica Bold", size: fontSize)!
        
        let now = NSDate()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let formatDate : NSString = formatter.string(from: now as Date) as NSString
        
        var endX = widthInPoints! - (20 * Xfactor * UIScreen.main.scale)
        let endY = heightInPoints! - (20 * Yfactor * UIScreen.main.scale)
        let point = CGPoint(x: 20 * Xfactor * UIScreen.main.scale , y: 30 * Yfactor * UIScreen.main.scale)
        
        print("\(12 * Xfactor * UIScreen.main.scale)")
        
        let starWidth = formatDate.widthOfString(usingFont: UIFont.systemFont(ofSize: fontSize))
        
        print("starWidth = \(starWidth)")
        
        endX -= starWidth
        let endPoint = CGPoint(x: endX, y: endY)
        
        print("endX = \(endX)")
        
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(image.size, false, scale)
        
        let textFontAttributes = [
            NSFontAttributeName: textFont,
            NSForegroundColorAttributeName: textColor,
            ] as [String : Any]
        image.draw(in: CGRect(origin: CGPoint.zero, size: image.size))
        
        let rect = CGRect(origin: point, size: image.size)
        text.draw(in: rect, withAttributes: textFontAttributes)
        
        let Rrect = CGRect(origin: endPoint, size: image.size)
        formatDate.draw(in: Rrect, withAttributes: textFontAttributes)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    @IBAction func dismiss(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveAction(_ sender: UIBarButtonItem) {
        
        let imageName = "/abcde.jpg"
//        print(imageName)
        var documentsDirectoryPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        documentsDirectoryPath += imageName
        print(documentsDirectoryPath)
        
        
        var paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let docs: String = paths[0] + "yourNameImg.jpg"
//        let fullPath = docs.stringByAppendingPathComponent("yourNameImg.png")
        print(docs)
        
        let settingsData: NSData = UIImageJPEGRepresentation(previewImage!, 1.0)! as NSData
        
        UIImageWriteToSavedPhotosAlbum(previewImage!, nil, nil, nil)
        
        settingsData.write(toFile: docs, atomically: true)
        
//        settingsData.write(toFile: documentsDirectoryPath, atomically: true)
        
        dismiss(sender)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        print("memoryWaring")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension NSString {
    
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSFontAttributeName: font]
        let size = self.size(attributes: fontAttributes)
        return size.width
    }
    
    func heightOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSFontAttributeName: font]
        let size = self.size(attributes: fontAttributes)
        return size.height
    }
}
